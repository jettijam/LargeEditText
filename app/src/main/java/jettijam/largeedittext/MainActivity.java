package jettijam.largeedittext;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView mTextView;
    EditText mEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextView = (TextView) findViewById(R.id.main_text_textview);
        mEditText = (EditText) findViewById(R.id.main_edit_edittext);

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("OnTextChanged", "s : "+s.toString());
                Log.d("OnTextChanged", "start : "+start);
                Log.d("OnTextChanged", "before : "+before);
                Log.d("OnTextChanged", "count : "+count);
                String htmlString = s.toString();
                try {
                    String string = s.toString();
                    String selectedString = string.substring(start, start + 1);
                    StringBuilder sb = new StringBuilder(string);
                    int idx = sb.lastIndexOf(selectedString);
                    htmlString = sb.replace(idx, idx+1, "<big><big><big>" + selectedString + "</big></big></big>").toString();
//                    htmlString = string.replace(selectedString, "<big><big><big>" + selectedString + "</big></big></big>");

                    Log.d("OnTextChanged", "SPANNED : " + htmlString);
                }catch (StringIndexOutOfBoundsException e){

                }
                mTextView.setText(Html.fromHtml(htmlString));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
